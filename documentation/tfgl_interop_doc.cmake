# Copyright (C) 2020, Inria, UCL, True Price, Peter Hedman
# All rights reserved.
# This software may be modified and distributed under the terms
# of the MIT license. See the LICENSE.md file for details.


set(PROJECT_PAGE "tfgl_interopPage")
set(PROJECT_LINK "https://gitlab.inria.fr/sibr/tfgl_interop")
set(PROJECT_DESCRIPTION "Tensorflow GL interoperability dependencies and cuda code")
set(PROJECT_TYPE "TOOLBOX")
