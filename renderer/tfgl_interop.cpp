/*
 * Copyright (C) 2020, Inria, UCL, True Price, Peter Hedman
 * All rights reserved.
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE.md file for details.
 */


// partly adapted from:
// https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/label_image/main.cc
// see also: https://www.tensorflow.org/versions/master/api_guides/cc/guide

#include "tfgl_interop.h"

#include <algorithm>
#include <chrono>
#include <fstream>
#include <sstream>
#include <vector>

#include <cassert>

#include <opencv2/opencv.hpp>

#include <Eigen/Core>

#include "tensorflow/cc/ops/standard_ops.h"
#include "tensorflow/core/framework/attr_value_util.h"
#include "tensorflow/core/framework/tensor.h"
#include "tensorflow/core/graph/node_builder.h"
#include "tensorflow/core/lib/io/path.h"
#include "tensorflow/core/platform/file_system.h"
#include "tensorflow/core/platform/init_main.h"
#include "tensorflow/core/public/session.h"
#include "tensorflow/core/util/command_line_flags.h"
#include "tensorflow/core/util/events_writer.h"

#include "custom_ops.h"

sibr::TF_GL::TF_GL(
	std::string graph_file_name,
	std::vector<std::shared_ptr<TexInfo>>& input_textures,
	std::vector<std::vector<int>> input_texture_mapping,
	std::vector<std::vector<int>> input_texture_channels_vec,
	const std::vector<std::string>& input_graph_node_names,
	std::vector<sibr::Texture2DRGBA32F::Ptr>& output_texture,
	const std::vector<std::string>& output_node_name,
	sibr::Window::Ptr window,
	bool NCHW,
	bool logging_enabled
) : _input_textures(input_textures),
_input_texture_mapping(input_texture_mapping),
_input_texture_channels_vec(input_texture_channels_vec),
_input_graph_node_names(input_graph_node_names),
_output_texture(output_texture),
_output_node_name(output_node_name),
_window(window),
_NCHW(NCHW)
{

	char* argv[] = { "program name", NULL };
	int argc = sizeof(argv) / sizeof(char*) - 1;

	const std::vector<tensorflow::Flag> flag_list;
	const std::string usage = tensorflow::Flags::Usage(argv[0], flag_list);
	if (!tensorflow::Flags::Parse(&argc, &argv[0], flag_list)) {
		LOG(ERROR) << usage;
		SIBR_ERR;
	}
	// We need to call this to set up global state for TensorFlow.
	tensorflow::port::InitMain(argv[0], &argc, (char***)& argv);
	if (argc > 1) {
		LOG(ERROR) << "Unknown argument " << argv[1] << "\n" << usage;
		SIBR_ERR;
	}

	std::unique_ptr<tensorflow::EventsWriter> logger;// = std::make_unique<tensorflow::EventsWriter>("dummy");

	const std::string log_folder = "/tf_logs/";
	if (logging_enabled) {
		tensorflow::Env::Default()->RecursivelyCreateDir(log_folder);
		logger.reset(new tensorflow::EventsWriter(
			tensorflow::io::JoinPath(log_folder, "events")));
	}

	LoadGraph(graph_file_name, logger);

};

void sibr::TF_GL::AddGraphInputsAndOutputOps(
	tensorflow::GraphDef* graph_def
) {
	// Replace placeholder ops with TextureInput ops.

	{
		for (tensorflow::NodeDef& node : *graph_def->mutable_node()) {
			for (int ign = 0; ign < _input_graph_node_names.size(); ign++) {
				if (node.name() == _input_graph_node_names[ign]) {

					const std::string& input_graph_node_name = _input_graph_node_names[ign];
					std::vector<int>& input_mapping = _input_texture_mapping[ign];
					std::vector<int>& input_texture_channels = _input_texture_channels_vec[ign];

					node.set_op("TextureInput");
					node.clear_attr();

					SetNodeAttr("GLFWwindow_ptr",
						reinterpret_cast<tensorflow::int64>(_window.get()), &node);

					SetNodeAttr<bool>("NCHW", _NCHW, &node);

					auto texture_ids = (*node.mutable_attr())["texture_ids"].mutable_list();
					auto texture_chs = (*node.mutable_attr())["texture_chs"].mutable_list();

					int tId = 0;
					for (const auto& t : input_mapping) {
						texture_ids->add_i(
							_input_textures[t]->handle
						);
						texture_chs->add_i(input_texture_channels[tId]);
						tId++;
					}

					const sibr::Vector2i input_shape(_input_textures[0]->w, _input_textures[0]->h);
					long int numChannels = std::accumulate(input_texture_channels.begin(), input_texture_channels.end(), 0);
					auto shape = (*node.mutable_attr())["shape"].mutable_shape();

					if (_NCHW) {
						shape->add_dim()->set_size(1);
						shape->add_dim()->set_size(numChannels);
						shape->add_dim()->set_size(input_shape[1]);
						shape->add_dim()->set_size(input_shape[0]);
					}
					else {
						shape->add_dim()->set_size(1);
						shape->add_dim()->set_size(input_shape[1]);
						shape->add_dim()->set_size(input_shape[0]);
						shape->add_dim()->set_size(numChannels);
					}

					std::cout << input_shape[1] << input_shape[0] << numChannels << std::endl;

				}
			}
		}
	}

	// Replace upsampling ops with our custom op. In your protobuf, you can do
	// this using tf.tile(x, [1, 1, 2, 2], name="model/UpsampleNN")
	{
		for (tensorflow::NodeDef& node : *graph_def->mutable_node()) {
			// Note that ResizeBilinear also has extra size inputs that we'll
			// ignore.
			if (StartsWith(node.name(), "model/UpsampleNN") &&
				!EndsWith(node.name(), "multiples")) {
				node.set_op("CudaNNUpsample");
				node.mutable_input()->RemoveLast();  // remove input from tf.tile()
				node.clear_attr();
			}
		}
	}

	// TODO (True): potential bug in Protobuf and/or the way this project uses the
	// Protobuf library? Have to force the release of fields before setting them
	// (they otherwise point to the same memory; this doesn't happen using bazel,
	// though...).
	for (int ot = 0; ot < _output_node_name.size(); ot++)
	{
		auto node = graph_def->add_node();
		node->release_name();
		node->release_op();
		node->set_name("output" + std::to_string(ot));
		node->set_op("CopyToTexture");
		node->add_input(_output_node_name[ot]);

		SetNodeAttr("GLFWwindow_ptr", reinterpret_cast<tensorflow::int64>(_window.get()),
			node);
		SetNodeAttr<bool>("NCHW", _NCHW, node);
		SetNodeAttr<tensorflow::int64>("texture_id", _output_texture[ot]->handle(), node);

	}
}

tensorflow::Status sibr::TF_GL::LoadGraph(
	const std::string& graph_file_name,
	std::unique_ptr<tensorflow::EventsWriter>& logger) {

	//cudaGLSetGLDevice(0);

	tensorflow::GraphDef graph_def;
	tensorflow::Status load_graph_status;// =
	ReadBinaryProto(tensorflow::Env::Default(), graph_file_name, &graph_def);

	if (!load_graph_status.ok()) {
		return tensorflow::errors::NotFound("Failed to load compute graph at '",
			graph_file_name, "'");
	}

	AddGraphInputsAndOutputOps(&graph_def);

	if (logger != nullptr) {
		auto graph_def_str = new std::string;
		graph_def.SerializeToString(graph_def_str);
		tensorflow::Event event;
		event.set_allocated_graph_def(graph_def_str);
		logger->WriteEvent(event);
	}
	auto options = tensorflow::SessionOptions();
		options.config.mutable_gpu_options()->set_per_process_gpu_memory_fraction(0.98);
		options.config.mutable_gpu_options()->set_allow_growth(true);
	//	_session = new tensorflow::Session();

	tensorflow::NewSession(options, &_session);
	return _session->Create(graph_def);
}


void sibr::TF_GL::UploadImageToTexture(const cv::Mat& image,
	sibr::Texture2DRGBA32F::Ptr& texture
) {

	GLuint texture_id = texture->handle();

	glBindTexture(GL_TEXTURE_2D, texture_id);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	// use fast 4-byte alignment always
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

	cv::Mat copy(image.rows, image.cols, CV_32FC4);
	if (image.type() == CV_8UC3) {
		const float INV_255 = 1.f / 255.f;
		for (int y = 0; y < image.rows; ++y) {
			for (int x = 0; x < image.cols; ++x) {
				cv::Vec3b c = image.at<cv::Vec3b>(y, x);
				copy.at<cv::Vec4f>(y, x) =
					cv::Vec4f(c[0] * INV_255, c[1] * INV_255, c[2] * INV_255, 1.f);
			}
		}
	}
	else if (image.type() == CV_16UC1) {
		const float INV = 1.f / 65535.f;
		for (int y = 0; y < image.rows; ++y) {
			for (int x = 0; x < image.cols; ++x) {
				ushort c = image.at<ushort>(y, x);
				copy.at<cv::Vec4f>(y, x) =
					cv::Vec4f(c * INV, c * INV, c * INV, 1.f);
			}
		}
	}
	else {
		std::cout << "Image format not supported ! Opencv .type(): " << image.type() << std::endl;
		SIBR_ERR;
	}

	// set length of one complete row in data (doesn't need to equal image.cols)
	glPixelStorei(GL_UNPACK_ROW_LENGTH, copy.step / copy.elemSize());

	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, copy.cols, copy.rows, GL_BGRA,
		GL_FLOAT, copy.data);

	glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);

	glBindTexture(GL_TEXTURE_2D, 0);
}

cv::Mat sibr::TF_GL::DownloadTexture(
	sibr::Texture2DRGBA32F::Ptr texture
) {

	GLuint texture_id = texture->handle();
	const sibr::Vector2i resolution(texture->w(), texture->h());

	cv::Mat image(resolution.y(), resolution.x(), CV_32FC4);

	glBindTexture(GL_TEXTURE_2D, texture_id);
	//We use BGRA because of the loading method that gives RGB istead of BGR
	glGetTexImage(GL_TEXTURE_2D, 0, GL_BGRA, GL_FLOAT, image.data);
	glBindTexture(GL_TEXTURE_2D, 0);

	cv::Mat image_uint8;
	image *= 255.f;
	image.convertTo(image_uint8, CV_8UC4);

	return image_uint8;
}

void sibr::TF_GL::run(std::vector<std::string> _run_node_names, std::vector<std::pair<std::string, tensorflow::Tensor>> ph) {
	const auto run_status = _session->Run(ph, {}, _run_node_names, {});
	if (!run_status.ok()) {
		LOG(ERROR) << run_status;
		SIBR_ERR;
	}
}

