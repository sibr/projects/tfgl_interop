/*
 * Copyright (C) 2020, Inria, UCL, True Price, Peter Hedman
 * All rights reserved.
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE.md file for details.
 */


#define NOSIBR 0
#include "CopyToTextureOp.h"

#include <cuda_gl_interop.h>
#include "core/graphics/Window.hpp"

#define CUDA_CHECK(ans) \
  { gpuAssert((ans), __FILE__, __LINE__); }

inline void gpuAssert(cudaError_t code, const char* file, int line,
	bool abort = true) {
	if (code != cudaSuccess) {
		fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file,
			line);
		if (abort) exit(code);
	}
}
#define CUDA_CHECK_ERROR CUDA_CHECK(cudaPeekAtLastError())

//------------------------------------------------------------------------------

CopyToTextureOp::CopyToTextureOp(tensorflow::OpKernelConstruction* context)
	: tensorflow::OpKernel(context) {
	tensorflow::int64 value;
	context->GetAttr("GLFWwindow_ptr", &value);
	window_ = sibr::Window::Ptr(reinterpret_cast<sibr::Window*>(value));
	window_->makeContextCurrent();

	context->GetAttr("texture_id",
		reinterpret_cast<tensorflow::int32*>(&texture_id_));

	context->GetAttr("NCHW", &_NCHW);

	window_->makeContextCurrent();

	cudaGraphicsGLRegisterImage(&cudaTexture_, texture_id_, GL_TEXTURE_2D,
		cudaGraphicsMapFlagsWriteDiscard);
	window_->makeContextNull();
}

CopyToTextureOp::~CopyToTextureOp() {
	window_->makeContextCurrent();
	cudaGraphicsUnregisterResource(cudaTexture_);
	window_->makeContextNull();
	CUDA_CHECK_ERROR
}

void CopyToTextureOp::Compute(tensorflow::OpKernelContext* context) {
	const tensorflow::Tensor& input_tensor = context->input(0);
	//    const auto stream =
	//        static_cast<stream_executor::cuda::CUDAStream*>(
	//            context->op_device_context()->stream()->implementation())
	//            ->cuda_stream();
	//    LOG(INFO) << "::" << stream;

	cudaDeviceSynchronize();
	if (window_->getContextCurrent() != window_->GLFW()) {
		window_->makeContextCurrent();
	}
	cudaGraphicsMapResources(1, &cudaTexture_);  //, stream);

	cudaArray_t texture_array;
	cudaGraphicsSubResourceGetMappedArray(&texture_array, cudaTexture_, 0, 0);

	cudaResourceDesc res_desc;
	memset(&res_desc, 0, sizeof(res_desc));
	res_desc.resType = cudaResourceTypeArray;
	res_desc.res.array.array = texture_array;

	cudaSurfaceObject_t out_surface;
	cudaCreateSurfaceObject(&out_surface, &res_desc);

	CopyToTexture(input_tensor.shape() ,input_tensor.flat<float>().data(), out_surface, _NCHW);

	cudaDestroySurfaceObject(out_surface);

	cudaGraphicsUnmapResources(1, &cudaTexture_);  //, stream);
	window_->makeContextNull();
	//

}
