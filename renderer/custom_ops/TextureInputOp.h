/*
 * Copyright (C) 2020, Inria, UCL, True Price, Peter Hedman
 * All rights reserved.
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE.md file for details.
 */


#ifndef TEXTURE_INPUTS_OP_H_
#define TEXTURE_INPUTS_OP_H_

#include <vector>


#include <GL/glew.h>
#include <GLFW/glfw3.h>
namespace sibr {
	class Window;
};

#include <cuda_runtime.h>

#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"

class TextureInputOp : public tensorflow::OpKernel {
 public:
  static const size_t MAX_NUM_INPUTS = 64;

  explicit TextureInputOp(tensorflow::OpKernelConstruction* context);

  ~TextureInputOp();

  void Compute(tensorflow::OpKernelContext* context) override;

  static void CopyToTensor(
      const tensorflow::TensorShape & shape, const std::vector<int>& channels,
      const std::vector<cudaTextureObject_t>& in_textures,
      float* out_tensor,
	  bool NCHW);

 private:
  std::vector<GLuint> textureIds_;
  std::vector<int> textureChs_;
  std::vector<cudaGraphicsResource_t> cudaTextures_;
  bool _NCHW;
  std::shared_ptr<sibr::Window> window_;
  tensorflow::TensorShape shape_;
  size_t numInputs_;
};

#endif  // TEXTURE_INPUTS_OP_H_
