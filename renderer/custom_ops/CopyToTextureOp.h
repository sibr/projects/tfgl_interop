/*
 * Copyright (C) 2020, Inria, UCL, True Price, Peter Hedman
 * All rights reserved.
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE.md file for details.
 */


#ifndef COPY_TO_TEXTURE_OP_H_
#define COPY_TO_TEXTURE_OP_H_

#include <GL/glew.h>
#include <GLFW/glfw3.h>

namespace sibr {
	class Window;
};


#include <cuda_runtime.h>

#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"

class CopyToTextureOp : public tensorflow::OpKernel {
 public:
  explicit CopyToTextureOp(tensorflow::OpKernelConstruction* context);

  ~CopyToTextureOp();

  void Compute(tensorflow::OpKernelContext* context) override;

  static void CopyToTexture(const tensorflow::TensorShape & shape,
                            const float* in_tensor,
                            cudaSurfaceObject_t out_texture,
							bool NCHW);

 private:
  GLuint texture_id_;
  bool _NCHW;
  std::shared_ptr<sibr::Window> window_;
  cudaGraphicsResource_t cudaTexture_;
};

#endif // COPY_TO_TEXTURE_OP_H_
