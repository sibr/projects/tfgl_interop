# Copyright (C) 2020, Inria, UCL, True Price, Peter Hedman
# All rights reserved.
# This software may be modified and distributed under the terms
# of the MIT license. See the LICENSE.md file for details.


find_package(CUDA REQUIRED)
link_directories(${CUDA_TOOLKIT_ROOT_DIR}/bin)

## Select the right version of the library based on the version of CUDA
SET(SIBR_TENSORFLOW_PATH "https://repo-sam.inria.fr/fungraph/dependencies/ibr-common/win3rdParty-MSVC15-splitted%20version/tensorflow.7z" INTERNAL)
SET(SIBR_PROTOBUF_PATH "https://repo-sam.inria.fr/fungraph/dependencies/ibr-common/win3rdParty-MSVC15-splitted%20version/protobuf.7z" INTERNAL)
if(CUDA_VERSION_MAJOR GREATER_EQUAL 10)
    SET(SIBR_TENSORFLOW_PATH "https://repo-sam.inria.fr/fungraph/dependencies/ibr-common/win3rdParty-MSVC15-splitted%20version/tensorflow_cuda10.7z" INTERNAL)
    SET(SIBR_PROTOBUF_PATH "https://repo-sam.inria.fr/fungraph/dependencies/ibr-common/win3rdParty-MSVC15-splitted%20version/protobuf_cuda10.7z" INTERNAL)
endif()
sibr_addlibrary(
    NAME tensorflow
    MSVC11 "${SIBR_TENSORFLOW_PATH}"
    MSVC14 "${SIBR_TENSORFLOW_PATH}"
    MULTI_SET
        CHECK_CACHED_VAR Tensorflow_DIR             PATH "tensorflow"
        CHECK_CACHED_VAR Tensorflow_LIBRARYDIR      PATH "tensorflow/lib"
        CHECK_CACHED_VAR Tensorflow_INCLUDE_DIRS    PATH "tensorflow/include"
        CHECK_CACHED_VAR Tensorflow_LIBRARIES       PATH "tensorflow/lib/tensorflow.lib"
)

sibr_addlibrary(
    NAME protobuf
    MSVC11 "${SIBR_PROTOBUF_PATH}"
    MSVC14 "${SIBR_PROTOBUF_PATH}"
    MULTI_SET
        CHECK_CACHED_VAR Protobuf_INCLUDE_DIRS      PATH "protobuf/include"
        CHECK_CACHED_VAR Protobuf_LIBRARIES         PATH "protobuf/lib/libprotobuf.lib"
)