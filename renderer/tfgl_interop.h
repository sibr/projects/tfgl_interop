/*
 * Copyright (C) 2020, Inria, UCL, True Price, Peter Hedman
 * All rights reserved.
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE.md file for details.
 */


#ifndef TFGL_INTEROP_H
#define TFGL_INTEROP_H

#include <memory>
#include <vector>

#include "core/graphics/Texture.hpp"
#include <GL/glew.h>

#include "core/graphics/Window.hpp"
#include <cuda_gl_interop.h>

#include "tensorflow/core/framework/tensor.h" 
#include "tensorflow/core/public/session.h"
namespace tensorflow
{
	class Status;
	class EventsWriter;
	class GraphDef;
	class NodeDef;
	class TensorShape;
}

namespace sibr
{

	struct TexInfo {
		GLuint handle;
		int w;
		int h;

		template<typename T_Type, unsigned int T_NumComp> TexInfo(std::shared_ptr<RenderTarget<T_Type,T_NumComp>> t, int targetId=0) : handle(t->handle(targetId)), w(t->w()), h(t->h()) {};
		template<typename T_Type, unsigned int T_NumComp> TexInfo(std::shared_ptr<Texture2D<T_Type,T_NumComp>> t) : handle(t->handle()), w(t->w()), h(t->h()) {};
		TexInfo() : handle(-1), w(-1), h(-1) {};

	};

	class TF_GL
	{

	public:
		SIBR_CLASS_PTR(TF_GL);
		tensorflow::Session * _session;
		// Vector containing the input texture info
		std::vector<std::shared_ptr<TexInfo>>&_input_textures;
		std::vector<std::vector<int>> _input_texture_mapping;
		std::vector<std::vector<int>> _input_texture_channels_vec;
		const std::vector<std::string>& _input_graph_node_names;
		std::vector<sibr::Texture2DRGBA32F::Ptr>& _output_texture;
		const std::vector<std::string>& _output_node_name;
		sibr::Window::Ptr _window;
		bool _NCHW;

		TF_GL(
			std::string graph_file_name,
		 	std::vector<std::shared_ptr<TexInfo>>& input_textures,
			std::vector<std::vector<int>> input_texture_mapping,
			std::vector<std::vector<int>> input_texture_channels_vec,
			const std::vector<std::string>& input_graph_node_names,
			std::vector<sibr::Texture2DRGBA32F::Ptr>& output_texture,
			const std::vector<std::string>& output_node_name,
			sibr::Window::Ptr window,
			bool NCHW = false,
			bool logging_enabled = false
		);

		void run(std::vector<std::string> _run_node_names, std::vector<std::pair<std::string, tensorflow::Tensor>> ph = {});

		sibr::Window::Ptr window() {
			return _window;
		}

	private:

		// Check if a string starts with another string.
		inline bool StartsWith(const std::string& str, const std::string& prefix) {
			return (str.size() >= prefix.size()) &&
				!str.compare(0, prefix.size(), prefix);
		};

		// Check if a string ends with another string.
		inline bool EndsWith(const std::string& str, const std::string& suffix) {
			return (str.size() >= suffix.size()) &&
				!str.compare(str.size() - suffix.size(), suffix.size(), suffix);
		};

		//------------------------------------------------------------------------------
		// Graph transform operations taken from
		// tensorflow:tensorflow/tools/graph_transforms/transform_utils.h

		// Inserts a value into a NodeDef's map of attributes.
		// This is a bit different than AddNodeAttr in node_def_util.h because it
		// overwrites any existing attributes with the same key.
		template <class T>
		inline void SetNodeAttr(const std::string& key, const T& value,
			tensorflow::NodeDef* node) {
			tensorflow::AttrValue attr_value;
			tensorflow::SetAttrValue(value, &attr_value);
			auto* attr_map = node->mutable_attr();
			(*attr_map)[key] = attr_value;
		};

		template <class T>
		inline void SetNodeTensorAttr(const std::string& key,
			const tensorflow::TensorShape& shape,
			const std::vector<T>& values,
			tensorflow::NodeDef* node) {
			const tensorflow::DataType dtype = tensorflow::DataTypeToEnum<T>::v();
			CHECK_EQ(shape.num_elements(), values.size());
			tensorflow::Tensor tensor(dtype, shape);
			T* dest_data = tensor.flat<T>().data();
			std::copy_n(values.data(), values.size(), dest_data);

			tensorflow::TensorProto tensor_proto;
			tensor.AsProtoTensorContent(&tensor_proto);
			SetNodeAttr(key, tensor_proto, node);
		};

		void AddGraphInputsAndOutputOps(
			tensorflow::GraphDef* graph_def
		);

		tensorflow::Status LoadGraph(
			const std::string& graph_file_name,
			std::unique_ptr<tensorflow::EventsWriter>& logger);


		void UploadImageToTexture(const cv::Mat& image,
			sibr::Texture2DRGBA32F::Ptr& texture
		);

		cv::Mat DownloadTexture(
			sibr::Texture2DRGBA32F::Ptr texture
		);

		
	};

}

#endif
