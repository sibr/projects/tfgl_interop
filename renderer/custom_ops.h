/*
 * Copyright (C) 2020, Inria, UCL, True Price, Peter Hedman
 * All rights reserved.
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE.md file for details.
 */


#include "tensorflow/core/framework/op.h"

#include "custom_ops/CopyToTextureOp.h"
#include "custom_ops/CudaBilinearUpsampleOp.h"
#include "custom_ops/CudaNNUpsampleOp.h"
#include "custom_ops/TextureInputOp.h"

REGISTER_OP("CopyToTexture")
.Attr("GLFWwindow_ptr: int")
.Attr("texture_id: int")
.Attr("NCHW: bool")
.Input("in_tensor: float");

REGISTER_KERNEL_BUILDER(Name("CopyToTexture").Device(tensorflow::DEVICE_GPU),
	CopyToTextureOp);

REGISTER_OP("CudaBilinearUpsample")
.Input("in_tensor: float")
.Output("out_tensor: float");

REGISTER_KERNEL_BUILDER(Name("CudaBilinearUpsample")
	.Device(tensorflow::DEVICE_GPU),
	CudaBilinearUpsampleOp);

REGISTER_OP("CudaNNUpsample")
.Input("in_tensor: float")
.Output("out_tensor: float");

REGISTER_KERNEL_BUILDER(Name("CudaNNUpsample").Device(tensorflow::DEVICE_GPU),
	CudaNNUpsampleOp);

REGISTER_OP("TextureInput")
.Attr("GLFWwindow_ptr: int")
.Attr("NCHW: bool")
.Attr("texture_ids: list(int)")
.Attr("texture_chs: list(int)")
.Attr("shape: shape")
.Output("out_tensor: float");

REGISTER_KERNEL_BUILDER(Name("TextureInput").Device(tensorflow::DEVICE_GPU),
	TextureInputOp);