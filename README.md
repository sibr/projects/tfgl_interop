
## Tensorflow/OpenGL Interop for SIBR {#tfgl_interopPage}

This repository contains an SIBR adaptation of the original code of True Price:

https://github.com/trueprice/tensorflow_opengl_interop/

This version is required for several projects of the SIBR system: http://sibr.gitlabpages.inria.fr.
It includes several additional wrappers and helper functions specific to SIBR.
